## Requirements:

* Python 2.7+
* Jinja2 >= 2.8
* CherryPy >= 4.0.0
* MarkupSafe >= 0.23
* PyYAML >= 3.11
* Git
* LibYaml-Dev
* Python-dev
* Build Essential
* Pip

## Instructions for Debian/Ubuntu:

1. Update APT

    `sudo apt-get update`

2. Install the needed software packages

    `sudo apt-get install build-essential python2.7 python-dev git python-pip libyaml-dev`

3. PIP install Python Modules

    `sudo pip install jinja2 cherrypy markupsafe PyYAML`

4. Clone the git repository (install MultiMonit)

    `sudo git clone git://github.com/desgyz/MultiMonit.git /opt/MultiMonit`

5. Fix MultiMonit Permissions to avoid problems

    `sudo chown -R user:group /opt/MultiMonit`

6. Run MultiMonit

    `sudo python2.7 /opt/MultiMonit/multimonit.py`

Goto ip-address:3005 for the setup to begin.

The systemd and init.d are located in the scripts folder.